package policeUAVs;

import java.util.ArrayList;
import java.util.Iterator;

import policeUAVs.Uav;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.query.space.grid.MooreQuery;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.SpatialMath;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.SimUtilities;
import java.lang.Iterable;

public class Uav {
	ContinuousSpace<Object> space;
	Grid<Object> grid;
	double heading;
	final double turnSpeed = 0.4;			
	final double DESIRED_DISTANCE = 0.5;
	final double separationWeight = RandomHelper.nextDoubleFromTo(0.4, 0.6);
	final double cohesionWeight = 1.0 - separationWeight;
	final double distance = 0.2;
	
	public Uav(ContinuousSpace<Object> space, Grid<Object> grid){
		this.space = space;
		this.grid = grid;
		heading = randomRadian();
	}
	
	/**
	 * The method that every Uav runs every tick.
	 */
	@ScheduledMethod(start = 1, interval = 1)
	public void step(){
		forward();
		double heading1 = averageTwoDirections(cohesionDirection(),separationDirection());
		heading1 = averageTwoDirections(heading1,heading);
		double deviation = averageTwoDirections(heading,randomRadian());
		double heading2 = averageTwoDirections(alignmentDirection(),deviation);
		heading2 = averageTwoDirections(heading2,heading);
		heading = averageTwoDirections(heading1,heading2);
		
	}
	
	/**
	 * This method returns a direction in which this Uav should travel in order to follow the Cohesion steering method
	 * @return heading--the new direction in radians
	 */
	public double cohesionDirection(){
		ArrayList<Uav> uavSet = neighborhood();
		if(uavSet.size() > 0){
			double avgUavDirection = uavSet.get(0).getHeading();
			double avgUavDistance = DESIRED_DISTANCE;
			for(Uav uav : uavSet){
				avgUavDirection = averageTwoDirections(uav.getHeading(),avgUavDirection);
				avgUavDistance = (avgUavDistance + distance(uav)) / 2;
			}
			if(avgUavDistance > DESIRED_DISTANCE){
				moveTowards(avgUavDirection,0.2*cohesionWeight);
			}
			return avgUavDirection;
		}else{
			return heading;
		}
	}
	
	/**
	 * This method returns a direction in which this Uav should travel in order to follow the Separation steering method
	 * @return heading--the direction in radians
	 */
	public double separationDirection(){
		ArrayList<Uav> uavSet = neighborhood();
		ArrayList<Uav> uavsTooClose = new ArrayList<Uav>();
		for(Uav uav : uavSet){
			if(distance(uav) < DESIRED_DISTANCE*0.5){
				uavsTooClose.add(uav);
			}
		}
		if(uavsTooClose.size() > 0){
			double avgUavDirection = heading;
			for(Uav uav : uavsTooClose){
				avgUavDirection = averageTwoDirections(uav.getHeading(),avgUavDirection);
			}
			double distanceToClosestUav = Double.MAX_VALUE;
			for(Uav uav : uavsTooClose){
				if(distance(uav) < distanceToClosestUav){
					distanceToClosestUav = distance(uav);
				}
			}
			if(distanceToClosestUav < DESIRED_DISTANCE*0.5){
				moveTowards(oppositeDirection(avgUavDirection), 0.2*separationWeight);
			}
			if(toMyLeft(avgUavDirection)){
				return (heading+turnSpeed)%Math.PI*2;
			}else{
				double newHeading = heading-turnSpeed;
				if(newHeading < 0)newHeading += Math.PI*2;
				return newHeading%Math.PI*2;
			}
		}
		return heading;
	}
	
	/**
	 * This method returns a direction in which this Boid should travel in order to follow the Separation steering method
	 * @return averageHeading--the direction in radians
	 */
	public double alignmentDirection(){
		ArrayList<Uav> uavSet = neighborhood();
		
		double averageHeading = heading;
		for(Uav uav : uavSet){
			averageHeading = averageTwoDirections(averageHeading,uav.getHeading());
		}
		return averageHeading;
	}

	/**
	 * 
	 * @param boid--A Uav object
	 * @return a double value denoting the distance in spatial units between the passed Uav object and the calling Uav object
	 */
	public double towards(Uav uav){
		NdPoint myPoint = space.getLocation(this);
		NdPoint otherPoint = space.getLocation(uav);
		
		return SpatialMath.calcAngleFor2DMovement(space, myPoint, otherPoint);
	}
	
	/**
	 * Returns the distance in spatial units between the calling Uav and the Uav passed as an argument.
	 * @param boid--a Uav object
	 * @return double value denoting distance
	 */
	public double distance(Uav uav) {
		NdPoint myPoint = space.getLocation(this);
		NdPoint otherPoint = space.getLocation(uav);
		double differenceX = Math.abs(myPoint.getX() - otherPoint.getX());
		double differenceY = Math.abs(myPoint.getY() - otherPoint.getY());
		return Math.hypot(differenceX, differenceY);
	}

	public double getHeading(){
		return heading;
	}
	
	public void setHeading(double heading){
		this.heading = heading;
	}
	
	/**
	 * Returns the direction halfway between one direction and another.
	 * @param dir1--first angle in radians
	 * @param dir2--second angle in radians
	 * @return a double value between 0 and 2*PI representing a direction
	 */
	public double averageTwoDirections(double angle1, double angle2){
		assert(angle1 <= Math.PI*2);
		assert(angle2 <= Math.PI*2);
		if(Math.abs(angle1-angle2) < Math.PI){
			return (angle1+angle2)/2;
		}
		else return oppositeDirection( (oppositeDirection(angle1) + oppositeDirection(angle2)) / 2 );
	}
	
	/**
	 * Returns the direction opposite to the one passed it, in radians.
	 * @param angle--a direction in radians
	 * @return the opposite direction as angle in radians
	 */
	public double oppositeDirection(double angle){
		assert(angle <= Math.PI*2);	
		angle = Math.toDegrees(angle);
		return Math.toRadians((angle + 180) % 360);
	}
	
	/**
	 * Moves the calling Uav some amount in a direction.
	 * @param direction--the direction to move the Uav
	 * @param dist--the amount to move the Uav
	 */
	public void moveTowards(double direction, double dist){
		assert(direction <= Math.PI*2);
		NdPoint pt = space.getLocation(this);
		double moveX = pt.getX() + Math.cos(heading)*dist;
		double moveY = pt.getY() + Math.sin(heading)*dist;
		space.moveTo(this, moveX, moveY);
	}
	
	/**
	 * Returns a double between 0 and 2*PI
	 * @return a radian between 0 and 2*PI
	 */
	public double randomRadian(){
		return Math.random()*(2*Math.PI);
	}
	
	/**
	 * Moves the calling Uav object an amount (defined by the Class-wide variable distance) 
	 * in the direction of the Boid's current heading.
	 */
	public void forward(){
		NdPoint pt = space.getLocation(this);
		double moveX = pt.getX() + Math.cos(heading)*distance;
		double moveY = pt.getY() + Math.sin(heading)*distance;
		space.moveTo(this, moveX, moveY);
		grid.moveTo(this, (int)moveX, (int)moveY);
	}
	
	/**
	 * Returns all Uav objects not farther than 1 units away from the calling Uav, not including the calling Uav.
	 * @return circularNeighborhood--an ArrayList of Uav objects
	 */
	public ArrayList<Uav> neighborhood(){
		MooreQuery<Uav> query = new MooreQuery(grid,this);
		Iterator<Uav> iter = query.query().iterator();
		ArrayList<Uav> uavSet = new ArrayList<Uav>();
		while(iter.hasNext()){
			uavSet.add(iter.next());
		}
		Iterable<Object> list = grid.getObjectsAt(grid.getLocation(this).getX(),grid.getLocation(this).getY());
		for(Object uav : list){
			uavSet.add((Uav) uav);
		}
		SimUtilities.shuffle(uavSet,RandomHelper.getUniform());
		uavSet.remove(this);
		ArrayList<Uav> circularNeighborhood = new ArrayList<Uav>();
		for(Uav uav : uavSet){
			if(distance(uav) <= 1.0){
				circularNeighborhood.add(uav);
			}
		}
		return circularNeighborhood;
	}
	
	/**
	 * @return a count of all neighbors
	 */
	public int neighborCount(){
		ArrayList<Uav> uavSet = neighborhood();
		return uavSet.size();
	}
	
	/**
	 * @param angle--a direction in radians
	 * @return boolean value denoting whether the angle passed as argument is on the left-hand side of the calling Uav.
	 */
	public boolean toMyLeft(double angle){
		double angleDegrees = Math.toDegrees(angle);
		double headingDegrees = Math.toDegrees(heading);
		if(Math.abs(angleDegrees - headingDegrees) < 180){
			if(angleDegrees < headingDegrees){
				return true;
			}else return false;
		}else{
			if(angleDegrees < headingDegrees){
				return false;
			}else return true;
		}
	}
	
	/**
	 * @return distance--the average distance of all neighboring Uavs.
	 */
	public double avgUavDistance(){
		double distance = DESIRED_DISTANCE;
		for(Uav uav : neighborhood()){
			distance = (distance(uav)+distance)/2;
		}
		return distance;
	}
}
